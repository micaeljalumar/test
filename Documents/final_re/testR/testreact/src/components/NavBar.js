import React, { Component } from "react";
import { Link } from 'react-router-dom';

export default class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle"
              data-toggle="collapse"
              data-target="#navigation-index"
            >
              <span className="sr-only">Toggle navigation</span>
              <span className="icon-bar" />
              <span className="icon-bar" />
              <span className="icon-bar" />
            </button>
          </div>

          <div className="collapse navbar-collapse" id="navigation-index">
            <ul className="nav navbar-nav navbar-right">
              <li>
                <Link to="/register">
                  <i className="material-icons">assignment_ind</i> Registrar
                </Link>
              </li>
              <li>
                <Link to="/login">
                  <i className="material-icons">account_circle</i> Entrar
                </Link>
              </li>
              <li>
                <Link to="/dashboard">
                  <i className="material-icons">dashboard</i> Dashboard
                </Link>
              </li>
              <li>
                <a href="http://demos.creative-tim.com/material-kit-pro/presentation.html?ref=utp-freebie">
                  <i className="material-icons">unarchive</i> Registrar c
                </a>
              </li>
              <li>
                <a
                  rel="tooltip"
                  title="Follow us on Twitter"
                  data-placement="bottom"
                  href="https://twitter.com/CreativeTim"
                  className="btn btn-white btn-simple btn-just-icon"
                >
                  <i className="fa fa-twitter" />
                </a>
              </li>
              <li>
                <a
                  rel="tooltip"
                  title="Like us on Facebook"
                  data-placement="bottom"
                  href="https://www.facebook.com/CreativeTim"
                  className="btn btn-white btn-simple btn-just-icon"
                >
                  <i className="fa fa-facebook-square" />
                </a>
              </li>
              <li>
                <a
                  rel="tooltip"
                  title="Follow us on Instagram"
                  data-placement="bottom"
                  href="https://www.instagram.com/CreativeTimOfficial"
                  className="btn btn-white btn-simple btn-just-icon"
                >
                  <i className="fa fa-instagram" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
