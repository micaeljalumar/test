import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="container">
          <nav className="pull-left">
            <ul>
              <li>
                <a href="#">Soporte</a>
              </li>
              <li>
                <a href="http://presentation.creative-tim.com">Sobre nosotros</a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">Blog</a>
              </li>
              <li>
                <a href="http://www.creative-tim.com/license">Terminos de licencia</a>
              </li>
            </ul>
          </nav>
          <div className="copyright pull-right">
            &copy; 2017 - 2018 {" "}
            <a href="#">
              Toro Tim
            </a>
          </div>
        </div>
      </footer>
    );
  }
}
