import React, { Component } from "react";
import { Route } from 'react-router-dom';

import NavBar from "./components/NavBar";
import Sigup from "./components/Sigup";
import Register from "./components/Register";
import Footer from "./components/Footer";
import "./App.css";


class App extends Component {
  render() {
    return (
      <div className="App">
        <NavBar />
        <div className="signup-page">
          <div className="wrapper">
            <div className="header header-filter" style={{backgroundImage: 'url(../assets/img/city.jpg)', backgroundSize: 'cover', backgroundPosition: 'top center'}}>
              <div className="container">
                <Route path="/login" component={Sigup}/>
                <Route path="/register" component={Register}/>
              </div>
              <Footer />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
