import React, { Component } from "react";
import { Scene, Stack, Router } from "react-native-router-flux";
import Home from "./components/Home";
import MapViewComponent from "./components/MapView";
const App = () => (
  <Router>
    <Stack key="root">
      <Scene key="home" component={Home} hideNavBar />
      <Scene key="MapView" component={MapViewComponent} />
    </Stack>
  </Router>
);

export default App;
