import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Animated,
  FlatList
} from "react-native";
import { Constants } from "expo";

//Components
import ResultCard from "./ResultCard";

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: false,
      result: null,
      query: "Maroon 5",
      animated: new Animated.Value(0)
    };
  }

  _keyExtractor = (item, index) => item.id;

  animation = () => {
    Animated.timing(this.state.animated, {
      duration: 800,
      toValue: 1
    }).start();
  };

  fetchAPI = () => {
    this.animation();
    let { query } = this.state;
    if (query == "") {
      alert("Enter a search parameter!");
      return false;
    }
    query = query.replace(/ /g, "");
    this.setState({ isLoading: true });
    fetch(`https://rest.bandsintown.com/artists/${query}/events?app_id=test`)
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          isLoading: false,
          result: responseJson
        });
        this.setState({ isLoading: false });
      })
      .catch(error => {
        this.setState({ isLoading: false });
        console.warn(error);
      });
  };

  render() {
    var interpolatedHeight = this.state.animated.interpolate({
      inputRange: [0, 1],
      outputRange: ["100%", "20%"]
    });
    return (
      <View style={styles.container}>
        <Animated.View style={[styles.header, { height: interpolatedHeight }]}>
          <Text style={styles.headerText}>Test</Text>
          <View style={styles.queryContainer}>
            <TextInput
              style={styles.inputQuery}
              onChangeText={query => this.setState({ query })}
              value={this.state.query}
            />
            <TouchableOpacity style={styles.btnSearch} onPress={this.fetchAPI}>
              <Text>Search</Text>
            </TouchableOpacity>
          </View>
        </Animated.View>
        <View style={styles.content}>
          {this.state.result ? (
            <FlatList
              style={{ flex: 1, width: "100%" }}
              data={this.state.result}
              keyExtractor={this._keyExtractor}
              renderItem={({ item }) => <ResultCard data={item} />}
            />
          ) : (
            this.state.isLoading && (
              <ActivityIndicator color={"white"} size={"large"} />
            )
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#2c3e50"
  },
  header: {
    alignItems: "center",
    justifyContent: "center",
    padding: 10
  },
  headerText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    padding: 5
  },
  queryContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  btnSearch: {
    height: 40,
    backgroundColor: "#ecf0f1",
    alignItems: "center",
    justifyContent: "center",
    padding: 5,
    borderRadius: 5,
    marginLeft: 5
  },
  inputQuery: {
    height: 40,
    width: 200,
    borderColor: "#2ecc71",
    borderWidth: 2,
    borderRadius: 8,
    padding: 1,
    color: "white"
  },
  content: {
    flex: 1,
    width: "100%",
    padding: 10,
    backgroundColor: "#2c3e50",
    justifyContent: "center",
    alignItems: "center"
  }
});
