import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
var moment = require("moment");

export default class ResultCard extends Component {
  render() {
    const impData = this.props.data.venue;
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph}>{impData.name}</Text>
        <View style={styles.mapView}>
          <TouchableOpacity
            style={styles.btnMap}
            onPress={() =>
              Actions.MapView({
                latitude: impData.latitude,
                longitude: impData.longitude,
                artist: impData.name
              })}
          >
            <MaterialIcons name="map" size={60} color="#2c3e50" />
          </TouchableOpacity>
        </View>
        <View style={styles.actions}>
          <View style={styles.btnActions}>
            <MaterialIcons name="location-city" size={20} color="#ecf0f1" />
            <Text
              style={styles.textAction}
            >{`${impData.region} ${impData.city}`}</Text>
          </View>
          <View style={styles.btnActions}>
            <MaterialIcons name="date-range" size={20} color="#ecf0f1" />
            <Text style={styles.textAction}>
              {moment.utc(this.props.data.datetime).fromNow()}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ecf0f1",
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
    width: "100%",
    backgroundColor: "#2ecc71",
    marginVertical: 5
  },
  paragraph: {
    padding: 10,
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    color: "white"
  },
  mapView: {
    width: "100%",
    height: 150,
    justifyContent: "center",
    alignItems: "center"
  },
  btnMap: {
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#27ae60",
    borderWidth: 3,
    borderColor: "#2c3e50"
  },
  actions: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#27ae60",
    paddingVertical: 10
  },
  btnActions: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  textAction: {
    color: "#ecf0f1",
    fontSize: 12
  }
});
