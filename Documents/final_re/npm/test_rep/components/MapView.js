import React from "react";
import { MapView } from "expo";

export default class MapViewComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { latitude, longitude } = this.props;
    return (
      <MapView
        style={{ flex: 1, padding: 20 }}
        initialRegion={{
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
      >
        <MapView.Marker
          coordinate={{
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude)
          }}
          title={`${this.props.artist} is here!!`}
        />
      </MapView>
    );
  }
}
